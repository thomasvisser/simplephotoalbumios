//
//  PhotoConverter.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import SwiftyJSON

class PhotoConverter {

    func convert(data: Any) throws -> [Photo] {
        let albumArray = JSON(data).arrayValue

        return albumArray.map {
            Photo(
                albumID: $0["albumId"].int,
                title: $0["title"].string,
                url: $0["url"].string,
                thumbnailUrl: $0["thumbnailUrl"].string
            )
        }
    }
}
