//
//  PhotoWebservice.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class PhotoWebservice {

    let webservice: Webservice = Webservice()
    let converter = PhotoConverter()

    func fetchPhotos(fromAlbumId albumId: Int, completion: @escaping (([Photo]?, WebserviceError) -> Void)) {
        let url = "https://jsonplaceholder.typicode.com/photos?albumId=\(albumId)"

        webservice.fetchUrl(url: url, completion: { [weak self] data, status in

            guard let photoData = data else {
                debugPrint("No photoData")
                completion(nil, WebserviceError.failed)
                return
            }

            do {
                let photos = try self?.converter.convert(data: photoData)
                completion(photos, WebserviceError.succes)
            } catch {
                debugPrint("Photo Conversion failed with error: \(error.localizedDescription)")
                completion(nil, WebserviceError.failed)
            }
        })
    }
}
