//
//  AlbumConverter.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import SwiftyJSON

class AlbumConverter {

    func convert(data: Any) throws -> [Album] {
        let albumArray = JSON(data).arrayValue

        return albumArray.map {
            Album(
                userID: $0["userId"].int,
                title: $0["title"].string
            )
        }
    }
}
