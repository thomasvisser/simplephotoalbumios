//
//  AlbumWebservice.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import SwiftyJSON

class AlbumWebservice {

    let webservice: Webservice = Webservice()
    let converter = AlbumConverter()

    func fetchAlbums(completion: @escaping (([Album]?, WebserviceError) -> Void)) {
        let url = "https://jsonplaceholder.typicode.com/albums/"

        webservice.fetchUrl(url: url, completion: { [weak self] data, status in

            guard let albumData = data else {
                completion(nil, WebserviceError.failed)
                return
            }

            do {
                let albums = try self?.converter.convert(data: albumData)
                completion(albums, WebserviceError.succes)
            } catch {
                debugPrint("Album Conversion failed with error: \(error.localizedDescription)")
                completion(nil, WebserviceError.failed)
            }
        })
    }

    func fetchAlbumPhotoCount(withAlbumId albumId: Int, completion: @escaping ((Int?, WebserviceError) -> Void)) {
        let url = "https://jsonplaceholder.typicode.com/photos?albumId=\(albumId)"

        webservice.fetchUrl(url: url, completion: { data, status in

            guard let photoData = data else {
                completion(nil, WebserviceError.failed)
                return
            }

            let amountOfPhotos = JSON(photoData).arrayValue.count
            completion(amountOfPhotos, WebserviceError.succes)
        })
    }
}
