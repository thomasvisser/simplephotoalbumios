//
//  Webservice.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import Alamofire

enum WebserviceError: Error {
    case succes
    case failed
}

class Webservice {

    func fetchUrl(url: String,  completion: @escaping ((Any?, WebserviceError) -> Void)) {

        guard let url = URL(string: url) else {
            debugPrint("Url Conversion failed")
            completion(nil, WebserviceError.failed)
            return
        }

        Alamofire.request(url, method: .get)
        .validate()
        .responseJSON { response in
            guard response.result.isSuccess else {
                completion(nil, WebserviceError.failed)
                return
            }

            guard let values = response.value else {
                debugPrint("No value in response")
                completion(nil, WebserviceError.failed)
                return
            }

            completion(values, WebserviceError.succes)
        }
    }
}
