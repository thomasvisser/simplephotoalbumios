//
//  UITableViewCell + reuseidentifier.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {

    public static func reuseIdentifier() -> String {
        return String(describing: self)
    }
}
