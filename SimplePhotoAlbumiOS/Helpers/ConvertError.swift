//
//  ConvertError.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

public struct ConverterError: Error {
    public var description: String

    public init(description: String = "Failed to convert") {
        self.description = description
    }
}
