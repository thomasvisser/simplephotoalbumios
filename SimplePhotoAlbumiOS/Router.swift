//
//  Router.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class Router {

    static func navigateToPhotos(withNavController navigationController: UINavigationController?, albumId: Int) {
        let photoListViewModel = PhotoListViewModel(relatedAlbumId: albumId)
        let photoListView = PhotoListViewController(viewModel: photoListViewModel)
        navigationController?.pushViewController(photoListView, animated: true)
    }

    static func navigateToPhotoDetail(withNavController navigationController: UINavigationController?, viewModel: PhotoViewModel) {
        let photoDetailViewModel = PhotoDetailViewModel(viewModel: viewModel)
        let photoDetailView = PhotoDetailViewController(viewModel: photoDetailViewModel)
        navigationController?.pushViewController(photoDetailView, animated: true)
    }
}
