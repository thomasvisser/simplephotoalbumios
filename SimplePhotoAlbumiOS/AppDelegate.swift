//
//  AppDelegate.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 05/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import UIKit
import Kingfisher

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let rootViewController = createInitialViewController()

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = rootViewController

        setupKingFisherProperties()

        return true
    }

    func createInitialViewController() -> UIViewController {

        let navigationController = UINavigationController(rootViewController: AlbumListViewController(viewModel: AlbumListViewModel()))
        navigationController.navigationBar.prefersLargeTitles = true

        return navigationController
    }

    func setupKingFisherProperties() {
        //Set KingFisher memory cache size to 500 MB
        KingfisherManager.shared.cache.memoryStorage.config.totalCostLimit = 500 * 1024 * 1024

        //Set KingFisher disk cache size to 1 GB
        KingfisherManager.shared.cache.diskStorage.config.sizeLimit = 1000 * 1024 * 1024

        //Set KingFisher disk expiration date to 10 minutes
        KingfisherManager.shared.cache.diskStorage.config.expiration = .seconds(600)
    }
}
