//
//  CanConfigureViews.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 05/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

public protocol CanConfigureViews {

    func configureViews()
    func configureViewProperties()
    func configureViewEvents()
    func configureViewHierarchy()
    func configureViewLayout()
}

public extension CanConfigureViews {

    func configureViews() {
        configureViewProperties()
        configureViewEvents()
        configureViewHierarchy()
        configureViewLayout()
    }

    func configureViewProperties() {}
    func configureViewEvents() {}
    func configureViewHierarchy() {}
    func configureViewLayout() {}
}
