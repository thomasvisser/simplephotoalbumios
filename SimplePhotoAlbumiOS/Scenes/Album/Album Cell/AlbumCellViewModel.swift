//
//  AlbumCellViewModel.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class AlbumCellViewModel {

    let album: AlbumViewModel

    init(album: AlbumViewModel) {
        self.album = album
    }

    var title: String? {
        return album.title
    }

    var thumbnailImage: UIImage? {
        return UIImage(named: "defaultPhotoIcon")
    }

    var amountOfPhotos: String {
        return String("\(album.amountOfPhotos ?? 0) photos")
    }
}
