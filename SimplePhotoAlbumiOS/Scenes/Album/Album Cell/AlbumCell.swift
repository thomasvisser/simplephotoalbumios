//
//  AlbumListCell.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 06/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import UIKit
import PureLayout

class AlbumCell: UITableViewCell {
    
    private let albumPreviewImageView: UIImageView = UIImageView()
    private let albumTitleLabel: UILabel = UILabel()
    private let photosCountLabel: UILabel = UILabel()

    private var viewModel: AlbumCellViewModel? = nil {
        didSet {
            configureViewProperties()
            configureViewLayout()
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        configureViews()
    }

    func configure(with viewModel: AlbumCellViewModel) {
        self.viewModel = viewModel
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AlbumCell: CanConfigureViews {

    func configureViewProperties() {
        accessoryType = .disclosureIndicator

        albumPreviewImageView.image = viewModel?.thumbnailImage
        albumPreviewImageView.contentMode = .scaleAspectFill

        albumTitleLabel.text = viewModel?.title
        albumTitleLabel.font = UIFont(name: ".SFUIText-Medium", size: 18)
        albumTitleLabel.numberOfLines = 1
        albumTitleLabel.textAlignment = .left

        photosCountLabel.text = viewModel?.amountOfPhotos
        photosCountLabel.font = UIFont(name: ".SFUIText-Medium", size: 15)
    }

    func configureViewHierarchy() {
        addSubview(albumPreviewImageView)
        addSubview(albumTitleLabel)
        addSubview(photosCountLabel)
    }

    func configureViewLayout() {
        albumPreviewImageView.autoSetDimensions(to: CGSize(width: 60, height: 60))
        albumPreviewImageView.autoPinEdge(toSuperviewEdge: .top)
        albumPreviewImageView.autoPinEdge(toSuperviewEdge: .bottom)
        albumPreviewImageView.autoPinEdge(toSuperviewEdge: .left, withInset: 20)

        albumTitleLabel.autoPinEdge(.left, to: .right, of: albumPreviewImageView, withOffset: 25)
        albumTitleLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 35)
        albumTitleLabel.autoAlignAxis(.horizontal, toSameAxisOf: self, withMultiplier: 0.6)

        photosCountLabel.autoPinEdge(.left, to: .right, of: albumPreviewImageView, withOffset: 25)
        photosCountLabel.autoAlignAxis(.horizontal, toSameAxisOf: self, withMultiplier: 1.4)
    }
}
