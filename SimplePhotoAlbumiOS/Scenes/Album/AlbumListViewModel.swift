//
//  AlbumListViewModel.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class AlbumListViewModel {

    var albums: [AlbumViewModel] = []
    private let webservice: AlbumWebservice

    var onAlbumsUpdated: (() -> ())?

    init() {
        webservice = AlbumWebservice()
    }

    var numberOfAlbums: Int {
        return albums.count
    }
}

extension AlbumListViewModel {

    func fetchAlbums() {
        webservice.fetchAlbums(completion: { [weak self] fetchedAlbums, status in

            if status == .failed {
                debugPrint("Failed to fetch albums")
                return
            }

            guard let albums = fetchedAlbums else { return }

            self?.createViewModels(models: albums)

            self?.fetchNumberOfPhotos()
        })
    }

    func fetchNumberOfPhotos() {

        //Create DispatchGroup to make sure code execution goes on when all fetchAlbumPhotoCount()
        // of all Albums are completed
        let dispatchGroup = DispatchGroup()

        for album in albums {
            dispatchGroup.enter()

            guard let albumId = album.albumId else { continue }
            
            webservice.fetchAlbumPhotoCount(withAlbumId: albumId, completion: { numberOfPhotos, status in

                if status == .failed {
                    debugPrint("Failed to fetch photos count")
                    return
                }

                album.amountOfPhotos = numberOfPhotos
                dispatchGroup.leave()
            })
        }

        //Waits for all fetchAlbumPhotoCount() fetches to complete
        dispatchGroup.notify(queue: .main) {
            self.onAlbumsUpdated?()
        }
    }

    private func createViewModels(models: [Album]) {
        albums = models.map {
            return AlbumViewModel(album: $0)
        }
    }
}
