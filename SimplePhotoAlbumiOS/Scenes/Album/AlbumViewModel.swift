//
//  AlbumViewModel.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

class AlbumViewModel {

    private let album: Album
    var amountOfPhotos: Int? = 0

    init(album: Album) {
        self.album = album
    }

    var title: String? {
        return album.title
    }

    var albumId: Int? {
        return album.userID
    }
}
