//
//  AlbumListViewController.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 05/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit
import PureLayout
import MBProgressHUD

class AlbumListViewController: UIViewController {

    private let tableView: UITableView = UITableView()
    private var loadingSpinner: MBProgressHUD = MBProgressHUD()
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(AlbumListViewController.reloadAlbums(_:)), for: .valueChanged)

        refreshControl.attributedTitle = NSAttributedString(string: "Reloading albums")
        return refreshControl
    }()

    private let viewModel: AlbumListViewModel

    init(viewModel: AlbumListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)

        configureViews()

        loadingSpinner.show(animated: true)
        viewModel.fetchAlbums()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func reloadAlbums(_ refreshControl: UIRefreshControl) {
        viewModel.fetchAlbums()
    }
}

extension AlbumListViewController: CanConfigureViews {

    func configureViewProperties() {
        navigationItem.title = "Albums"
        view.backgroundColor = .white

        loadingSpinner.mode = MBProgressHUDMode.indeterminate
        loadingSpinner.label.text = "Loading albums"

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(AlbumCell.self, forCellReuseIdentifier: AlbumCell.reuseIdentifier())
        tableView.isHidden = true
    }

    func configureViewEvents() {
        viewModel.onAlbumsUpdated = { [weak self] in
            self?.tableView.reloadData()
            self?.loadingSpinner.hide(animated: true)
            self?.tableView.isHidden = false

            //sleep 1 second so it looks like it is actually updating for the user
            DispatchQueue.main.async {
                sleep(1)
                self?.refreshControl.endRefreshing()
            }
        }
    }

    func configureViewHierarchy() {
        view.addSubview(loadingSpinner)
        view.addSubview(tableView)
        tableView.addSubview(refreshControl)
    }

    func configureViewLayout() {
        tableView.autoPinEdgesToSuperviewEdges()
    }
}

extension AlbumListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let albumListCell = tableView.dequeueReusableCell(withIdentifier: AlbumCell.reuseIdentifier(), for: indexPath) as? AlbumCell

        guard let albumCell = albumListCell else { return UITableViewCell() }
        albumCell.configure(with: AlbumCellViewModel(album: viewModel.albums[indexPath.row]))

        return albumCell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfAlbums
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let albumViewModel = viewModel.albums[indexPath.row]

        guard let albumId = albumViewModel.albumId else { return }
        Router.navigateToPhotos(withNavController: self.navigationController, albumId: albumId)

        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
