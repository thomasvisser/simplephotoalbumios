//
//  PhotoZoomViewController.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 13/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class PhotoZoomViewController: UIViewController {

    let scrollView: UIScrollView = UIScrollView()
    private let imageView: UIImageView

    private var imageViewLeftConstraint: NSLayoutConstraint?
    private var imageViewRightConstraint: NSLayoutConstraint?
    private var imageViewTopConstraint: NSLayoutConstraint?
    private var imageViewBottomConstraint: NSLayoutConstraint?

    init(image: UIImage?) {
        imageView = UIImageView(image: image)
        super.init(nibName: nil, bundle: nil)
        configureViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func updateMinZoomScaleForSize(_ size: CGSize) {
        let widthScale = size.width / imageView.bounds.width
        let heightScale = size.height / imageView.bounds.height
        let minScale = min(widthScale, heightScale)

        scrollView.minimumZoomScale = minScale
        scrollView.zoomScale = minScale
    }

    fileprivate func updateConstraintsForSize(_ size: CGSize) {

        let yOffset = max(0, (size.height - imageView.frame.height) / 2)
        imageViewTopConstraint?.constant = yOffset
        imageViewBottomConstraint?.constant = yOffset

        let xOffset = max(0, (size.width - imageView.frame.width) / 2)
        imageViewLeftConstraint?.constant = xOffset
        imageViewRightConstraint?.constant = xOffset

        view.layoutIfNeeded()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateMinZoomScaleForSize(view.bounds.size)
    }
}

extension PhotoZoomViewController: CanConfigureViews {

    func configureViewProperties() {
        scrollView.backgroundColor = .black
        scrollView.maximumZoomScale = 3
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self

        let zoomImageViewTapEvent = UITapGestureRecognizer(target: self, action: #selector(PhotoZoomViewController.dismissView(_:)))
        zoomImageViewTapEvent.delegate = self
        zoomImageViewTapEvent.numberOfTapsRequired = 1

        scrollView.addGestureRecognizer(zoomImageViewTapEvent)
    }

    func configureViewHierarchy() {
        view.addSubview(scrollView)
        scrollView.addSubview(imageView)
    }

    func configureViewLayout() {
        scrollView.autoPinEdgesToSuperviewEdges()

        imageViewLeftConstraint = imageView.autoPinEdge(toSuperviewEdge: .left)
        imageViewRightConstraint = imageView.autoPinEdge(toSuperviewEdge: .right)
        imageViewTopConstraint = imageView.autoPinEdge(toSuperviewEdge: .top)
        imageViewBottomConstraint = imageView.autoPinEdge(toSuperviewEdge: .bottom)
    }
}

extension PhotoZoomViewController: UIScrollViewDelegate {

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        updateConstraintsForSize(view.bounds.size)
    }
}

extension PhotoZoomViewController: UIGestureRecognizerDelegate {

    @objc func dismissView(_ sender: UITapGestureRecognizer?) {
        notifyParent()

        removeFromParent()
        view.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
}

extension PhotoZoomViewController {

    func notifyParent() {
        guard let parentView = parent as? PhotoDetailViewController else { return }
        parentView.animatePhotoToNormalState()
    }
}
