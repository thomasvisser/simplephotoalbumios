//
//  PhotoDetailViewController.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 13/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import PureLayout

class PhotoDetailViewController: UIViewController {

    private var imageView: UIImageView = UIImageView()
    private let titleLabel: UILabel = UILabel()
    private var photoZoomView: PhotoZoomViewController?
    private var imageViewTapEvent: UITapGestureRecognizer?
    private var foreGround: UIView = UIView()

    private let viewModel: PhotoDetailViewModel

    init(viewModel: PhotoDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        configureViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PhotoDetailViewController: CanConfigureViews {

    func configureViewProperties() {
        navigationItem.title = "Photo Detail"
        view.backgroundColor = .white

        foreGround.frame = view.frame
        foreGround.backgroundColor = .black
        foreGround.isHidden = true
        addAnimation(toView: foreGround, withDuration: 0.2, animationType: .fade)

        imageView.contentMode = .scaleAspectFit
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: viewModel.url)
        imageView.isUserInteractionEnabled = true

        imageViewTapEvent = UITapGestureRecognizer(target: self, action: #selector(PhotoDetailViewController.photoTapped(_:)))
        imageViewTapEvent?.delegate = self
        imageViewTapEvent?.numberOfTapsRequired = 1

        if let tapEvent = imageViewTapEvent {
            imageView.addGestureRecognizer(tapEvent)
        }

        titleLabel.text = viewModel.title
        titleLabel.font = UIFont(name: ".SFUIText-Medium", size: 20)
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
    }

    func configureViewHierarchy() {
        view.addSubview(titleLabel)
        view.addSubview(foreGround)
        view.addSubview(imageView)
    }

    func configureViewLayout() {
        imageView.autoSetDimensions(to: CGSize(width: view.frame.width, height: view.frame.width))
        imageView.autoPinEdge(toSuperviewMargin: .top)
        imageView.autoPinEdge(toSuperviewEdge: .left)
        imageView.autoPinEdge(toSuperviewEdge: .right)

        foreGround.autoPinEdgesToSuperviewEdges()

        titleLabel.autoPinEdge(.top, to: .bottom, of: imageView, withOffset: 30)
        titleLabel.autoAlignAxis(toSuperviewAxis: .vertical)
        titleLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 35)
        titleLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: 35)
    }
}

extension PhotoDetailViewController: UIGestureRecognizerDelegate {

    @objc func photoTapped(_ sender: UITapGestureRecognizer?) {
        animatePhotoToZoomState()
    }
}

extension PhotoDetailViewController {

    func animatePhotoToZoomState() {

        foreGround.isHidden = false

        //Animate imageView to the middle of the screen
        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
            let viewHeight = self.view.bounds.height
            let imageViewMiddleY: CGFloat = (viewHeight / 2) - (self.imageView.bounds.height / 2)
            self.imageView.frame = CGRect(x: self.imageView.bounds.minX, y: imageViewMiddleY, width: self.imageView.bounds.width, height: self.imageView.bounds.height)

            self.navigationController?.navigationBar.alpha = 0

        }, completion: { [unowned self] (_) in
            //Transition photo to zoomed ViewController
            self.photoZoomView = PhotoZoomViewController(image: self.imageView.image)

            guard let zoomView = self.photoZoomView else { return }
            self.view.addSubview(zoomView.view)
            self.addChild(zoomView)
            self.photoZoomView?.didMove(toParent: self)
        })
    }

    func animatePhotoToNormalState() {

        foreGround.isHidden = true

        //Animate imageView to the original position
        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
            let imageViewY: CGFloat = self.view.bounds.minY
            self.imageView.frame = CGRect(x: self.imageView.bounds.minX, y: imageViewY, width: self.imageView.bounds.width, height: self.imageView.bounds.height)

            self.navigationController?.navigationBar.alpha = 1
        })
    }

    func addAnimation(toView view: UIView, withDuration duration: CFTimeInterval, animationType: CATransitionType) {
        let animation: CATransition = CATransition()
        animation.type = animationType
        animation.duration = duration
        view.layer.add(animation, forKey: nil)
    }
}
