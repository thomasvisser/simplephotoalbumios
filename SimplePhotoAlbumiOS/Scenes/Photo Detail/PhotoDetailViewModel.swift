//
//  PhotoDetailViewModel.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 13/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

class PhotoDetailViewModel {

    private let viewModel: PhotoViewModel

    init(viewModel: PhotoViewModel) {
        self.viewModel = viewModel
    }

    var title: String? {
        return viewModel.title
    }

    var url: URL? {
        guard let tempUrl = viewModel.url else { return nil }
        return URL(string: tempUrl)
    }
}
