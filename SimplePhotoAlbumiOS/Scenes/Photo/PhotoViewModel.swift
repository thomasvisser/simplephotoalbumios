//
//  PhotoViewModel.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

class PhotoViewModel {

    private let photo: Photo

    init(photo: Photo) {
        self.photo = photo
    }

    var title: String? {
        return photo.title
    }

    var url: String? {
        return photo.url
    }

    var thumbnailUrl: String? {
        return photo.thumbnailUrl
    }
}
