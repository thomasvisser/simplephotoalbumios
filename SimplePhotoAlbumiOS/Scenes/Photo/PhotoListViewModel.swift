//
//  PhotoListViewModel.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

class PhotoListViewModel {

    private var photos: [PhotoViewModel] = []
    private let webservice: PhotoWebservice
    private let relatedAlbumId: Int

    var onPhotosUpdated: (() -> ())?

    init(relatedAlbumId: Int) {
        self.relatedAlbumId = relatedAlbumId
        webservice = PhotoWebservice()
    }

    var numberOfPhotos: Int {
        return photos.count
    }

    func photoViewModel(at index: Int) -> PhotoViewModel {
        return photos[index]
    }
}

extension PhotoListViewModel {

    func fetchPhotos() {
        webservice.fetchPhotos(fromAlbumId: relatedAlbumId, completion: { [weak self] fetchedPhotos, status in

            if status == .failed {
                debugPrint("Failed to fetch photos")
                return
            }

            guard let photos = fetchedPhotos else { return }

            debugPrint("Fetched Photos!!")
            self?.createViewModels(models: photos)
            self?.onPhotosUpdated?()
        })
    }

    private func createViewModels(models: [Photo]) {
        photos = models.map {
            return PhotoViewModel(photo: $0)
        }
    }
}
