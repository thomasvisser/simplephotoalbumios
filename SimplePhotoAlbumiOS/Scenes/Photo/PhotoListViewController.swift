//
//  PhotoListViewController.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class PhotoListViewController: UIViewController {

    private let tableView: UITableView = UITableView()
    private var loadingSpinner: MBProgressHUD = MBProgressHUD()
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(PhotoListViewController.reloadAlbums(_:)), for: .valueChanged)

        refreshControl.attributedTitle = NSAttributedString(string: "Reloading photos")
        return refreshControl
    }()

    private let viewModel: PhotoListViewModel
    
    init(viewModel: PhotoListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)

        configureViews()

        loadingSpinner.show(animated: true)
        viewModel.fetchPhotos()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func reloadAlbums(_ refreshControl: UIRefreshControl) {
        viewModel.fetchPhotos()
    }
}

extension PhotoListViewController: CanConfigureViews {

    func configureViewProperties() {
        navigationItem.largeTitleDisplayMode = .never
        navigationItem.title = "Photos"
        view.backgroundColor = .white

        loadingSpinner.mode = MBProgressHUDMode.indeterminate
        loadingSpinner.label.text = "Loading photos"

        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(PhotoCell.self, forCellReuseIdentifier: PhotoCell.reuseIdentifier())
        tableView.isHidden = true
    }

    func configureViewEvents() {
        viewModel.onPhotosUpdated = { [weak self] in
            self?.tableView.reloadData()
            self?.loadingSpinner.hide(animated: true)
            self?.tableView.isHidden = false

            //sleep 1 second so it looks like it is actually updating for the user
            DispatchQueue.main.async {
                sleep(1)
                self?.refreshControl.endRefreshing()
            }
        }
    }

    func configureViewHierarchy() {
        view.addSubview(loadingSpinner)
        view.addSubview(tableView)
        tableView.addSubview(refreshControl)
    }

    func configureViewLayout() {
        tableView.autoPinEdgesToSuperviewEdges()
    }
}

extension PhotoListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let photoListCell = tableView.dequeueReusableCell(withIdentifier: PhotoCell.reuseIdentifier(), for: indexPath) as? PhotoCell

        guard let photoCell = photoListCell else { return UITableViewCell() }
        photoCell.configure(with: PhotoCellViewModel(photo: viewModel.photoViewModel(at: indexPath.row)))

        return photoCell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfPhotos
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let photoViewModel = viewModel.photoViewModel(at: indexPath.row)
        Router.navigateToPhotoDetail(withNavController: self.navigationController, viewModel: photoViewModel)

        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
