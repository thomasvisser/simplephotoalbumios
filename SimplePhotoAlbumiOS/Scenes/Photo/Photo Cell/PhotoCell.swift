//
//  PhotoCell.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import UIKit
import PureLayout
import Kingfisher

class PhotoCell: UITableViewCell {

    private var photoPreviewImageView: UIImageView = UIImageView()
    private let photoTitleLabel: UILabel = UILabel()

    private var viewModel: PhotoCellViewModel? = nil {
        didSet {
            configureViewProperties()
            configureViewLayout()
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        configureViews()
    }

    func configure(with viewModel: PhotoCellViewModel) {
        self.viewModel = viewModel
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PhotoCell: CanConfigureViews {

    func configureViewProperties() {
        accessoryType = .disclosureIndicator

        photoPreviewImageView.contentMode = .scaleAspectFill
        photoPreviewImageView.kf.indicatorType = .activity
        photoPreviewImageView.kf.setImage(with: viewModel?.thumbnailUrl)

        photoTitleLabel.text = viewModel?.title
        photoTitleLabel.font = UIFont(name: ".SFUIText-Medium", size: 18)
        photoTitleLabel.numberOfLines = 2
        photoTitleLabel.textAlignment = .left
    }

    func configureViewHierarchy() {
        self.addSubview(photoPreviewImageView)
        self.addSubview(photoTitleLabel)
    }

    func configureViewLayout() {
        photoPreviewImageView.autoSetDimensions(to: CGSize(width: 60, height: 60))
        photoPreviewImageView.autoPinEdge(toSuperviewEdge: .top)
        photoPreviewImageView.autoPinEdge(toSuperviewEdge: .bottom)
        photoPreviewImageView.autoPinEdge(toSuperviewEdge: .left, withInset: 20)

        photoTitleLabel.autoPinEdge(.left, to: .right, of: photoPreviewImageView, withOffset: 25)
        photoTitleLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 35)
        photoTitleLabel.autoAlignAxis(toSuperviewMarginAxis: .horizontal)
    }
}
