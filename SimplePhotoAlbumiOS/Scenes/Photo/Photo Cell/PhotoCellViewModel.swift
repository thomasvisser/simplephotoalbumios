//
//  PhotoCellViewModel.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 08/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation
import UIKit

class PhotoCellViewModel {

    let photo: PhotoViewModel

    init(photo: PhotoViewModel) {
        self.photo = photo
    }

    var title: String? {
        return photo.title
    }

    var thumbnailUrl: URL? {
        guard let url = photo.thumbnailUrl else { return nil }
        return URL(string: url)
    }
}
