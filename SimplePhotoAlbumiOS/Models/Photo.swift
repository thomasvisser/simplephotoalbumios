//
//  Photo.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

struct Photo {
    var albumID: Int?
    var title: String?
    var url: String?
    var thumbnailUrl: String?

    init(albumID: Int?, title: String?, url: String?, thumbnailUrl: String?) {
        self.albumID = albumID
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
    }
}
