//
//  Album.swift
//  SimplePhotoAlbumiOS
//
//  Created by Thomas Visser on 07/03/2019.
//  Copyright © 2019 Thomas Visser. All rights reserved.
//

import Foundation

struct Album {
    var userID: Int?
    var title: String?

    init(userID: Int?, title: String?) {
        self.userID = userID
        self.title = title
    }
}
